﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace CSharp.Helper
{
    public static class Extensions
    {
        #region DataRow.IsEmpty()
        /// <summary>
        /// Returns false if value of any of columns of the row are not empty or NULL. 
        /// </summary>
        /// <param name="ODataRow">DataRow</param>
        /// <returns>bool</returns>
        public static bool IsEmpty(this DataRow ODataRow)
        {
            bool Result = true;

            foreach (object OValue in ODataRow.ItemArray)
                if (OValue.ToString() != string.Empty)
                { Result = false; break; }

            return Result;
        }
        #endregion

        #region String.IsEmpty()
        /// <summary>
        /// Returns true if value is NULL or empty. 
        /// </summary>
        /// <param name="Ostring">string</param>
        /// <returns>bool</returns>
        public static bool IsNullOrEmpty(this string Ostring)
        {
            return (Ostring == null || Ostring.Trim().Length == 0);
        }

        /// <summary>
        /// Returns true if value is Neither NULL nor empty. 
        /// </summary>
        /// <param name="Ostring">string</param>
        /// <returns>bool</returns>
        public static bool IsNotNullOrEmpty(this string Ostring)
        {
            return !string.IsNullOrEmpty(Ostring);
        }
        #endregion

        #region DateTime.Compare(bool IsAscending, DateTime ComparedValue)
        /// <summary>
        /// Returns comparison between two datetime values depending on IsAscending. 
        /// </summary>
        /// <param name="ODataRow">DataRow</param>
        /// <returns>bool</returns>
        public static int Compare(this DateTime ODateTime, bool IsAscending, DateTime ComparedValue)
        {
            if (IsAscending)
                return ODateTime.CompareTo(ComparedValue);
            else
                return ComparedValue.CompareTo(ODateTime);
        }
        #endregion

        #region DataTable.AddColumns(string[] ColumnNames)
        /// <summary>
        /// Adds the list of columns to the Datatable.
        /// </summary>
        /// <param name="ODataTable">DataTable</param>
        /// <param name="ColumnNames">string[]</param>
        public static void AddColumns(this DataTable ODataTable, string[] ColumnNames)
        {
            foreach (string OColumnName in ColumnNames)
                ODataTable.Columns.Add(OColumnName);
        }
        #endregion

        #region Enum.StringValue()
        /// <summary>
        /// Returns the string value for the enum value
        /// </summary>
        /// <param name="EnumValue">Enum</param>
        /// <returns>string</returns>
        //public static string StringValue(this Enum EnumValue)
        //{
        //    Type OType = EnumValue.GetType();

        //    FieldInfo OFieldInfo = OType.GetField(EnumValue.ToString());
        //    StringValue[] OStringValues = OFieldInfo.GetCustomAttributes(typeof(StringValue), false) as StringValue[];

        //    return (OStringValues.Length > 0) ? OStringValues[0].Value : "";
        //}
        #endregion

        #region DataTable.RemoveRowsWithValues(string[] columnNames)
        /// <summary>
        /// Removes rows with the values from the datatable.
        /// </summary>
        /// <param name="oDataTable">DataTable</param>
        /// <param name="columnName">string</param>
        /// <param name="values">string[]</param>
        public static void RemoveRowsWithValues(this DataTable oDataTable, string columnName, string[] values)
        {
            if (oDataTable.Columns.Contains(columnName))
            {
                int RowCount = oDataTable.Rows.Count - 1;
                for (int Counter = RowCount; Counter >= 0; Counter--)
                    if (values.Contains<string>(oDataTable.Rows[Counter][columnName].ToString()))
                        oDataTable.Rows[Counter].Delete();
                oDataTable.AcceptChanges();
            }
            else
                throw new Exception("Column with name " + columnName + " not found.");
        }
        #endregion

        #region Object.IsNull()
        /// <summary>
        /// Returns true if the object is null.
        /// </summary>
        /// <param name="oObject">object</param>
        /// <returns>bool</returns>
        public static bool IsNull(this object oObject)
        {
            return oObject == null ? true : false;
        }
        #endregion

        #region Object.IsNotNull()
        /// <summary>
        /// Returns true if the object is not null.
        /// </summary>
        /// <param name="oObject">object</param>
        /// <returns>bool</returns>
        public static bool IsNotNull(this object oObject)
        {
            return oObject == null ? false : true;
        }
        #endregion

        #region Object[].IsNullOrEmpty()
        /// <summary>
        /// Returns true if the object array is null or empty.
        /// </summary>
        /// <param name="oObject">object</param>
        /// <returns>bool</returns>
        public static bool IsNullOrEmpty(this object[] oObject)
        {
            return (oObject == null || oObject.Length == 0);
        }
        #endregion

        #region DataRowView.IsEmpty()
        /// <summary>
        /// Returns false if value of any of columns of the row view are not empty or NULL. 
        /// </summary>
        /// <param name="oDataRowView">DataRowView</param>
        /// <returns>bool</returns>
        public static bool IsEmpty(this DataRowView oDataRowView)
        {
            bool Result = true;

            foreach (object OValue in oDataRowView.Row.ItemArray)
                if (OValue.ToString() != string.Empty)
                { Result = false; break; }

            return Result;
        }
        #endregion

        #region DataTable.AddBlankRow()
        /// <summary>
        /// Adds a blank row to the datatable.
        /// </summary>
        /// <param name="oDataTable">DataTable</param>
        public static void AddBlankRow(this DataTable oDataTable)
        {
            oDataTable.Rows.Add(new object[] { });
        }
        #endregion

        #region DataTable.EnsureRow()
        /// <summary>
        /// Adds a blank row to the datatable if no rows exist.
        /// </summary>
        /// <param name="oDataTable">DataTable</param>
        public static void EnsureRow(this DataTable oDataTable)
        {
            if (oDataTable.Rows.Count == 0)
                oDataTable.Rows.Add(new object[] { });
        }
        #endregion

        #region DataTable.IsEmpty()
        /// <summary>
        /// Returns false if at least one row exists in the table.
        /// </summary>
        /// <param name="oDataTable"></param>
        /// <returns></returns>
        public static bool IsNullorEmpty(this DataTable oDataTable)
        {
            if (oDataTable.IsNull() || oDataTable.Rows.Count == 0)
                return true;
            else
                return false;
        }
        #endregion

        #region string.ReplacePrefixWildCard()
        /// <summary>
        /// 
        /// </summary>
        /// <param name="oString">string</param>
        public static string ReplacePrefixWildCard(this string oString)
        {
            if (!string.IsNullOrEmpty(oString) && oString.IndexOf('*') == 0)
            {
                char[] modifiedString = oString.ToCharArray();
                modifiedString.SetValue('%', 0);
                oString = new string(modifiedString);
            }
            return oString.Replace("*", "");
        }
        #endregion

        #region AddReplace
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyList"></param>
        /// <param name="customProperty"></param>
        public static void AddReplace(this List<Attribute> propertyList, Attribute customProperty)
        {
            foreach (Attribute listedProperty in propertyList)
            {
                if (listedProperty.Equals(customProperty))
                {
                    propertyList.Remove(listedProperty);
                    break;
                }
            }
            propertyList.Add(customProperty);
        }
        #endregion

        #region Contains
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyList"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public static bool Contains(this Attribute[] propertyList, string propertyName)
        {
            bool contains = false;
            for (int Counter = 0; Counter <= propertyList.Length - 1; Counter++)
            {
                if (((Attribute)propertyList.GetValue(Counter)).ToString() == propertyName)
                {
                    contains = true;
                    break;
                }
            }
            return contains;
        }
        #endregion

        #region ToHexadecimalString
        /// <summary>
        /// Returns hexadecimal string from byte array.
        /// </summary>
        /// <param name="bytes"byte array></param>
        /// <returns>hexadecimal string</returns>
        public static string ToHexadecimalString(this byte[] bytes)
        {
            return bytes.Select(b => b.ToString("x2")).Aggregate((a, b) => a + b);
        }
        #endregion

        #region FromHexadecimalToBytes
        /// <summary>
        /// Returns byte array from a hexadecimal string
        /// </summary>
        /// <param name="hexadecimalString">hexadecimal string</param>
        /// <returns>byte array</returns>
        public static byte[] FromHexadecimalToBytes(this string hexadecimalString)
        {
            var bytes = new byte[hexadecimalString.Length / 2];

            for (var i = 0; i < hexadecimalString.Length; i += 2)
            {
                bytes[i / 2] = byte.Parse(hexadecimalString.Substring(i, 2), System.Globalization.NumberStyles.HexNumber);
            }

            return bytes;
        }
        #endregion

        #region string.GetDateValue
        /// <summary>
        /// Returns true if the string is null or empty.
        /// </summary>
        /// <param name="source">string</param>
        /// <returns>bool</returns>
        public static DateTime? GetDateValue(this string source)
        {
            DateTime? result = null;
            DateTime parseResult;
            if (source.IsNullOrEmpty())
                result = null;
            else if (DateTime.TryParse(source, out parseResult))
                result = parseResult;
            return result;
        }
        #endregion

        #region DataRow.ContainsColumn(string columnName)
        /// <summary>
        /// Returns True if the column exists in the datarow
        /// </summary>
        /// <param name="oDataRow">DataRow</param>
        /// <param name="columnName">string</param>
        public static bool ContainsColumn(this DataRow oDataRow, string columnName)
        {
            return oDataRow.Table.Columns.Contains(columnName);
        }
        #endregion

        #region DateTime.IsMinValue
        public static bool IsMinValue(this DateTime source)
        {
            return source.Equals(DateTime.MinValue);
        }
        #endregion

        #region DateTime.IsMaxValue
        public static bool IsMaxValue(this DateTime source)
        {
            return source.Equals(DateTime.MaxValue);
        }
        #endregion

        #region DateTime?.IsMinValue
        public static bool IsMinValue(this DateTime? source)
        {
            return source.HasValue ? source.Value.Equals(DateTime.MinValue) : false;
        }
        #endregion

        #region DateTime?.IsMaxValue
        public static bool IsMaxValue(this DateTime? source)
        {
            return source.HasValue ? source.Value.Equals(DateTime.MaxValue) : false;
        }
        #endregion

        #region NewtonSoft Conversions

        /// <summary>
        /// Convert Generic to Json String
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">source</param>
        /// <returns>string</returns>
        public static string ToJson<T>(this T source)
        {
            try
            {
                return JsonConvert.SerializeObject(source);
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        #endregion

        #region Time Conversion

        /// <summary>
        /// Get Time string in AM/PM format
        /// </summary>
        /// <param name="source">TimeSpan</param>
        /// <returns>string</returns>
        public static string GetTimeAmPm(this TimeSpan source)
        {
            string result = string.Empty;
            if (source.IsNotNull())
                result = new DateTime().Add(source).ToString(Constants.OP_TIMEFORMAT_AMPM);
            return result;
        }

        /// <summary>
        /// Get Time string in AM/PM format
        /// </summary>
        /// <param name="source">TimeSpan?</param>
        /// <returns>string</returns>
        public static string GetTimeAmPm(this TimeSpan? source)
        {
            string result = string.Empty;
            if (source.HasValue)
                result = new DateTime().Add(source.Value).ToString(Constants.OP_TIMEFORMAT_AMPM);
            return result;
        }

        /// <summary>
        /// Get Time string in 24hrs format
        /// </summary>
        /// <param name="source">TimeSpan</param>
        /// <returns>string</returns>
        public static string GetTime24HRS(this TimeSpan source)
        {
            string result = string.Empty;
            if (source.IsNotNull())
                result = new DateTime().Add(source).ToString(Constants.OP_TIMEFORMAT_24HRS);
            return result;
        }

        /// <summary>
        /// Get Time string in 24hrs format
        /// </summary>
        /// <param name="source">TimeSpan?</param>
        /// <returns>string</returns>
        public static string GetTime24HRS(this TimeSpan? source)
        {
            string result = string.Empty;
            if (source.HasValue)
                result = new DateTime().Add(source.Value).ToString(Constants.OP_TIMEFORMAT_24HRS);
            return result;
        }

        #endregion


        #region SQL DB Date Conversion

        /// <summary>
        /// Get Date string as specified format
        /// </summary>
        /// <param name="source">DateTime?</param>
        /// <param name="dateformat">string</param>
        /// <returns>string</returns>
        public static string GetDateFormatString(this DateTime? source, string dateformat)
        {
            string result = string.Empty;
            if (source.HasValue)
            {
                try
                {
                    result = source.Value.ToString(dateformat);
                }
                catch (Exception) { }
            }
            return result;
        }

        /// <summary>
        /// Get Date string in SQL DB format (YYYY-MM-DD)
        /// </summary>
        /// <param name="source">string</param>
        /// <returns>string</returns>
        public static string GetDBDateString(this string source)
        {
            string result = string.Empty;
            DateTime _date;
            if (source.IsNotNullOrEmpty())
            {
                if (DateTime.TryParse(source, out _date))
                {
                    result = _date.ToString(Constants.DBINPUT_DATE_FORMAT);
                }
            }
            return result;
        }

        /// <summary>
        /// Get Date string in SQL DB format (YYYY-MM-DD)
        /// </summary>
        /// <param name="source">DateTime?</param>
        /// <returns>string</returns>
        public static string GetDBDateString(this DateTime? source)
        {
            string result = string.Empty;
            if (source.HasValue)
                result = source.Value.ToString(Constants.DBINPUT_DATE_FORMAT);
            return result;
        }

        /// <summary>
        /// Get DateTime string in SQL DB format (YYYY-MM-DD HH:MM:SS)
        /// </summary>
        /// <param name="source">string</param>
        /// <returns>string</returns>
        public static string GetDBDateTimeString(this string source)
        {
            string result = string.Empty;
            DateTime _date;
            if (source.IsNotNullOrEmpty())
            {
                if (DateTime.TryParse(source, out _date))
                {
                    result = _date.ToString(Constants.DBINPUT_DATETIME_FORMAT);
                }
            }
            return result;
        }

        /// <summary>
        /// Get DateTime string in SQL DB format (YYYY-MM-DD HH:MM:SS)
        /// </summary>
        /// <param name="source">DateTime?</param>
        /// <returns>string</returns>
        public static string GetDBDateTimeString(this DateTime? source)
        {
            string result = string.Empty;

            if (source.HasValue)
            {
                result = source.Value.ToString(Constants.DBINPUT_DATETIME_FORMAT);
            }

            return result;
        }


        #endregion
    }

    class Constants
    {
        public const string OP_TIMEFORMAT_24HRS = @"HH:mm:ss";
        public const string OP_TIMEFORMAT_AMPM = @"hh:mm tt";
        public const string DBINPUT_DATE_FORMAT = @"yyyy-MM-dd";
        public const string DBINPUT_DATETIME_FORMAT = @"yyyy-MM-dd HH:mm:ss";
    }
}