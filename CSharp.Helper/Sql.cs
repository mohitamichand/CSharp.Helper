﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Reflection;

namespace CSharp.Helper
{
    public class Sql
    {
        /// <summary>
        /// Convert object to Guid
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>Guid</returns>
        public static Guid ToGuid(object value)
        {
            Guid _return = Guid.Empty;
            Guid.TryParse(Convert.ToString(value), out _return);
            return _return;
        }

        /// <summary>
        /// Convert object to int
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>int</returns>
        public static int ToInt(object value)
        {
            int _return = 0;
            int.TryParse(Convert.ToString(value), out _return);
            return _return;
        }

        /// <summary>
        /// Convert object to DateTime
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>DateTime</returns>
        public static DateTime ToDateTime(object value)
        {
            DateTime _return;
            DateTime.TryParse(Convert.ToString(value), out _return);
            return _return;
        }

        /// <summary>
        /// Convert object to DateTime?
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>DateTime?</returns>
        public static DateTime? ToNullDateTime(object value)
        {
            DateTime? _return = null;
            DateTime _checkParseValue;
            if (DateTime.TryParse(Convert.ToString(value), out _checkParseValue))
            {
                _return = _checkParseValue;
            }
            return _return;
        }

        /// <summary>
        /// Convert object to string
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>string</returns>
        public static string ToString(object value)
        {
            if (value != null)
                return Convert.ToString(value);
            else
                return "";
        }

        /// <summary>
        /// Convert List<Generic> to DataTable
        /// </summary>
        /// <param name="value">List<Generic></param>
        /// <returns>DataTable</returns>
        public static DataTable ToDataTable<T>(IList<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }

        /// <summary>
        /// Convert object to TimeSpan
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>TimeSpan</returns>
        public static TimeSpan ToTimeSpan(object value)
        {
            TimeSpan _return;
            TimeSpan.TryParse(Convert.ToString(value), out _return);
            return _return;
        }

        /// <summary>
        /// Convert object to bool
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>bool</returns>
        public static bool ToBool(object value)
        {
            bool _return = false;
            int _returnInt = 0;
            if (Int32.TryParse(Convert.ToString(value), out _returnInt))
            {
                _return = Convert.ToBoolean(_returnInt);
            }
            else {
                _return = Convert.ToBoolean(Convert.ToString(value));
            }

            return _return;
        }

        /// <summary>
        /// Convert object to Long
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>Long</returns>
        public static long ToLong(object value)
        {
            long _return = 0;
            long.TryParse(Convert.ToString(value), out _return);
            return _return;
        }

        /// <summary>
        /// Convert object to decimal
        /// </summary>
        /// <param name="value">object</param>
        /// <returns>decimal</returns>
        public static decimal ToDecimal(object value)
        {
            decimal _return = 0;
            decimal.TryParse(Convert.ToString(value), out _return);
            return _return;
        }
    }
}
