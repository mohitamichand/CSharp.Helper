﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CSharp.Helper
{
    public class SQLConnection
    {
        private readonly SqlConnection _con;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="connectionString">DB Connection String</param>
        public SQLConnection(string connectionString)
        {
            _con = new SqlConnection(connectionString);
        }

        #region CommonMethods
        /// <summary>
        /// Execute Query and returns rows affected counts
        /// </summary>
        /// <param name="query">string</param>
        /// <returns>int</returns>
        public int ExecuteNonQuery(string query)
        {
            if (_con.State == ConnectionState.Open)
                _con.Close();
            SqlCommand cmd = new SqlCommand(query, _con);
            _con.Open();
            int rowsAffected = cmd.ExecuteNonQuery();
            _con.Close();
            if (_con.State == ConnectionState.Closed)
                _con.Close();
            return rowsAffected;
        }

        /// <summary>
        /// Execute SP with params(optional) and returns rows affected counts
        /// </summary>
        /// <param name="sp">string</param>
        /// <param name="mapParam">Hashtable(Optional) Default = null</param>
        /// <returns>int</returns>
        public int ExecuteNonQuerySP(string sp, Hashtable mapParam = null)
        {
            int _retVal = 0;
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(sp, _con);
            cmd.CommandType = CommandType.StoredProcedure;

            if (mapParam != null)
            {
                foreach (string item in mapParam.Keys)
                {
                    cmd.Parameters.AddWithValue(item, mapParam[item]);
                }
            }
            _retVal = cmd.ExecuteNonQuery();
            _con.Close();
            return _retVal;
        }

        /// <summary>
        /// Execute Query and returns object
        /// </summary>
        /// <param name="query">string</param>
        /// <returns>Object</returns>
        public object ExecuteNGetValue(string query)
        {
            if (_con.State == ConnectionState.Open)
                _con.Close();
            SqlCommand cmd = new SqlCommand(query, _con);
            _con.Open();
            var obj = cmd.ExecuteScalar();
            _con.Close();

            if (_con.State == ConnectionState.Closed)
                _con.Close();

            return obj;
        }

        #endregion

        #region DataTable

        /// <summary>
        /// Get DataTable for query passed as parameter
        /// </summary>
        /// <param name="query">string</param>
        /// <returns>DataTable</returns>
        public DataTable GetDataTable(string query)
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(query);
            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(query, _con);
            ad.Fill(dt);
            _con.Close();
            return dt;
        }

        /// <summary>
        /// Get DataTable for sp(stored procedure) and parameters arrays
        /// </summary>
        /// <param name="sp">string</param>
        /// <param name="arrParams">object[]</param>
        /// <returns>DataTable</returns>
        public DataTable GetDataTable(string sp, object[] arrParams)
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(sp, _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(cmd);

            if (arrParams != null)
            {
                for (int i = 0; i < arrParams.Length; i++)
                {
                    cmd.Parameters[i + 1].Value = arrParams[i];
                }
            }

            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            _con.Close();
            return dt;
        }

        /// <summary>
        /// Get DataTable for sp(stored procedure) and parameters list
        /// </summary>
        /// <param name="sp">string</param>
        /// <param name="mapParam">Hashtable(Optional) Default = null</param>
        /// <returns>DataTable</returns>
        public DataTable GetDataTable(string sp, Hashtable mapParam = null)
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(sp, _con);
            cmd.CommandType = CommandType.StoredProcedure;

            if (mapParam != null)
            {
                foreach (string item in mapParam.Keys)
                {
                    cmd.Parameters.AddWithValue(item, mapParam[item]);
                }
            }

            DataTable dt = new DataTable();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(dt);
            _con.Close();
            return dt;
        }

        #endregion

        #region DataSet

        /// <summary>
        /// Get DataSet for sp(stored procedure) and parameters array
        /// </summary>
        /// <param name="sp">string</param>
        /// <param name="arrParams">object[]</param>
        /// <returns>DataSet</returns>
        public DataSet GetDataSet(string sp, object[] arrParams)
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(sp, _con);
            cmd.CommandType = CommandType.StoredProcedure;
            SqlCommandBuilder.DeriveParameters(cmd);

            if (arrParams != null)
            {
                for (int i = 0; i < arrParams.Length; i++)
                {
                    cmd.Parameters[i + 1].Value = arrParams[i];
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(ds);
            _con.Close();
            return ds;
        }

        /// <summary>
        /// Get DataSet for sp(stored procedure) and parameters list
        /// </summary>
        /// <param name="sp">string</param>
        /// <param name="mapParam">Hashtable(Optional) Default = null</param>
        /// <returns>DataSet</returns>
        public DataSet GetDataSet(string sp, Hashtable mapParam = null)
        {
            if (_con.State == ConnectionState.Closed)
                _con.Open();
            SqlCommand cmd = new SqlCommand(sp, _con);
            cmd.CommandType = CommandType.StoredProcedure;

            if (mapParam != null)
            {
                foreach (string item in mapParam.Keys)
                {
                    cmd.Parameters.AddWithValue(item, mapParam[item]);
                }
            }

            DataSet ds = new DataSet();
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            ad.Fill(ds);
            _con.Close();
            return ds;
        }

        #endregion
        
    }
}
